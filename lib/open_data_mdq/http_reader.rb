require 'open_data_mdq/reader'
require 'net/http'
require 'uri'
# url de la web: http://appsvr.mardelplata.gob.ar/consultas/appcontainer/appcontainer.asp?app=ORDENESCOMPRA
# detalle de orden: http://appsvr.mardelplata.gob.ar/Consultas07/OrdenesDeCompra/OC/detalle_OC.asp?fmNRO_OC=806&ANIO=2013&BOTON=b
# url del formulario: http://appsvr.mardelplata.gob.ar/Consultas07/OrdenesDeCompra/OC/index.asp

module OpenDataMdq
  class HttpReader < Reader
    attr_accessor :year, :jurisdiction, :tipocont, :code_number #, :provider_action #, :providers_page_count
    attr_reader :elements_options
    URL_FORM          = "http://appsvr.mardelplata.gob.ar/consultas/appcontainer/appcontainer.asp?app=ORDENESCOMPRA"
    URL_ORDER_DETAIL  = "http://appsvr.mardelplata.gob.ar/Consultas07/OrdenesDeCompra/OC/detalle_OC.asp?"
    URL_ACTION_FORM   = "http://appsvr.mardelplata.gob.ar/Consultas07/OrdenesDeCompra/OC/index.asp?"
    URL_PROVIDERS     = "http://appsvr.mardelplata.gob.ar/consultas07/proveedores/navegador/listado.asp"
    #http://www.mardelplata.gov.ar/consultas07/proveedores/navegador/fila_tabla.asp?cod=4933
    FRM_ANIO          = "fmANIO_CON"
    FRM_JURISDICTION  = "fmJURISDICCION_CON"
    FRM_TIPOCON       = "fmTIPOCONT_CON"
    FRM_NRO_OC        = "fmNRO_OC"
    FRM_ANIO_OC       = "ANIO"
    def initialize(args)
      args                  = defaults.merge(args)
      @year                 = args[:year]
      @jurisdiction         = args[:jurisdiction]
      @tipocont             = args[:tipocont]
      @code_number          = args[:nrooc]
      # @provider_action      = args[:provider_action]
      @providers_page_count = args[:providers_page_count]
      @elements_options     = {}
    end
    def defaults
      {:tipocont => "--", :providers_page_count => nil}
    end
    def process_order_detail
      params = {}
      params[FRM_ANIO_OC]       = @year
      params[FRM_NRO_OC]        = @code_number
      uri = "#{URL_ORDER_DETAIL}#{params.to_a.collect{|a| a.join('=')}.join('&')}&BOTON=b"
      puts "URL A CONSULTAR: #{URL_ORDER_DETAIL}#{params.to_a.collect{|a| a.join('=')}.join('&')}&BOTON=b"
      response = Net::HTTP.get(URI(uri))
      super(response)
    end
    def process_orders
      params                    = process_orders_defaults
      params[FRM_ANIO]          = year
      params[FRM_JURISDICTION]  = jurisdiction
      params[FRM_TIPOCON]       = tipocont
      puts "URL A CONSULTAR: #{URL_ACTION_FORM}#{params.to_a.collect{|a| a.join('=')}.join('&')}"
      response = Net::HTTP.get(URI("#{URL_ACTION_FORM}#{params.to_a.collect{|a| a.join('=')}.join('&')}"))
      super(response, jurisdiction)
    end
    def process_providers
      providers   = []
      data = nil
      total_pages = @providers_page_count || get_total_providers_pages
      total_pages.times do |page|
        if page > 0
          params = {}
          params["action"] = page+1
          url = "#{URL_PROVIDERS}?#{params.to_a.collect{|a| a.join('=')}.join('&')}"
          puts "PROCESANDO: #{url}"
          response = Net::HTTP.get(URI(url))
        else
          puts "PROCESANDO: #{URL_PROVIDERS}"
          response = Net::HTTP.get(URI(URL_PROVIDERS))
        end
        data = super(response, providers, total_pages)
        providers = data[:providers]
      end
      data
    end
    private
    # hay que buscar una manera mas prolija de buscar el total de paginas.
    def get_total_providers_pages
      params            = {}
      params["action"]  = @provider_action
      response          = Net::HTTP.get(URI(URL_PROVIDERS))
      data = super(response)
      data
    end

    def process_orders_defaults
      {"Consultar" => "Consultar"}
    end
  end
end
