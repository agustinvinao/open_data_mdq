require 'open_data_mdq/reader'
module OpenDataMdq
  class FileReader < Reader
    attr_accessor :source_path, :filenames, :data_result, :year, :sub_jurisdiction_id, :nrooc
    def initialize(args)
      args                  = defaults.merge(args)
      @year                 = args[:year]
      @sub_jurisdiction_id  = args[:sub_jurisdiction_id]
      @tipocont             = args[:tipocont]
      @nrooc                = args[:nrooc]
      @source_path          = args[:source_path]
      @filenames            = args[:filenames]
    end
    def defaults
      {:tipocont => "--"}
    end
    def process_providers
      providers = []
      data = nil
      # iteramos por los archivos
      @filenames.each do |filename|
        file = File.open("#{source_path}#{filename}")
        data = file.read
        data = super(data, providers, @filenames.size)
        file.close
      end
      data
    end
    def process_order_detail
      file = File.open("#{source_path}#{filename}")
      data = file.read
      data = super(data)
      file.close
      data
    end
    def process_orders
      file = File.open("#{source_path}#{filename}")
      data = file.read
      data = super(data, sub_jurisdiction_id)
      file.close
      data
    end
  end
end
