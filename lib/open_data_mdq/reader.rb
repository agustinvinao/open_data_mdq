require 'nokogiri'
require 'date'
require 'pp'
module OpenDataMdq
  class Reader
    PROVIDER_RAZON_SOCIAL   = 0
    PROVIDER_CUIT           = 1
    PROVIDERS_HEADERS       = [PROVIDER_RAZON_SOCIAL, PROVIDER_CUIT]

    D_ITEM_NUMBER           = 0
    D_DESCRIPTION           = 1
    D_UNIT_PRICE            = 2
    D_CANTIDAD              = 3
    D_MEASURE_UNIT          = 4
    D_TOTAL                 = 5

    P_DESCRIPCION           = 0
    P_PROCEDENCIA           = 4
    P_ESTIMADO              = 6
    P_MODIFICACION          = 7
    P_VIGENTE               = 8
    P_DEVENGADO             = 9
    P_RECIBIDO              = 11
    PRESUPUESTO_HEADERS = [P_PROCEDENCIA, P_ESTIMADO, P_MODIFICACION, P_VIGENTE, P_DEVENGADO, P_RECIBIDO]
    HEADER_NROOC            = 1
    HEADER_FECHAEMISION     = 2
    HEADER_IMPORTE          = 3
    HEADER_PROVEEDOR        = 4
    HEADER_TIPOCONTRATACION = 5
    HEADER_OBS              = 6
    HEADER_ESTADO           = 7
    HEADERS                 = [HEADER_NROOC, HEADER_FECHAEMISION, HEADER_IMPORTE, HEADER_PROVEEDOR, HEADER_TIPOCONTRATACION, HEADER_OBS, HEADER_ESTADO]
    DATA_NAME = :name
    DATA_PO   = :purchased_orders
    attr_accessor :datasrc, :data_result, :providers_page_count
    def initialize
      @data_result = []
    end
    def process_order_detail(dataOriginal)
      @data_result = []
      pdata = Nokogiri::HTML.parse(dataOriginal).to_html
      data = Nokogiri::HTML(pdata)
      unless has_data?(data.search("samp.subtitle"), "DetalleOrdenCompra")
        items = data.search("table tr")
        items.each do |item|
          if (row = item.css("td")).size > 0
            add_detail_orden_compra(create_detail_order_compra(row))
          end
        end
      end
    end
    def process_orders(dataOriginal, sub_jurisdiction_id)
      @data_result = []
      pdata = Nokogiri::HTML.parse(dataOriginal).to_html
      data = Nokogiri::HTML(pdata)

      unless has_data?(data.search(".descripcion"), "OrdenCompra")
        items = data.css("div.subtitle")
        items.each do |item|
          html_tag_subjurisdiction_id = get_subjurisdiction_id(item)
          recruit_trs                 = data.css(get_subjurisdiction_id_key(html_tag_subjurisdiction_id))
          #agregamos la nueva subjurisdiction si no existe en data
          unless has_subjurisdiction?(sub_jurisdiction_id)
            sub_jurisdiction = create_sub_jurisdictions(sub_jurisdiction_id, item.css('strong').text)
          else
            sub_jurisdiction = get_subjurisdiction(sub_jurisdiction_id)
          end
          add_subjurisditcion(sub_jurisdiction)
          recruit_trs.each do |recruit_tr|
            #creamos la nueva orden de compra
            if oc = create_ordenes_compra(recruit_tr.css("td"))
              #agregamos la orden de compra al proveedor correspondiente
              add_orden_compra(oc, sub_jurisdiction)
            end
          end
        end
      else
        puts "NO SE ENCONTRARON ORDENES"
      end
    end
    def process_providers(dataOriginal, providers, total_pages)
      @data_result = providers || []
      pdata = Nokogiri::HTML.parse(dataOriginal).to_html
      data = Nokogiri::HTML(pdata)
      unless has_data?(data.search("table tr th"), "providers")
        items = data.search("table tr")
        items.each do |item|
          if (row = item.css("td")).size > 0
            add_provider(row) if is_valid_row_provider?(row)
          end
        end
      else
        puts "NO HAY PROVEEDORES"
      end
      {total_pages: total_pages || get_total_providers_pages(dataOriginal), providers: @data_result}
    end
    def get_total_providers_pages(dataOriginal)
      page_count = 0
      pdata = Nokogiri::HTML.parse(dataOriginal).to_html
      data = Nokogiri::HTML(pdata)
      if (page_count_info = data.search("form").last.search("table tr td").children.first)
        page_count = page_count_info.text.split("de")[1].to_i
      end
      page_count
    end
    private




    def is_valid_row_provider?(row)
      !(row[PROVIDER_RAZON_SOCIAL].nil? || row[PROVIDER_CUIT].nil?)
    end
    def add_provider(row)
      @data_result << create_provider(row)
    end
    def add_detail_orden_compra(detail_orden_compra)
       @data_result << detail_orden_compra
    end
    def create_detail_order_compra(row)
      OrderCompraDetail.new(row[D_ITEM_NUMBER].text,
                            row[D_DESCRIPTION].text,
                            row[D_UNIT_PRICE].text.gsub("$", "").gsub(".","").gsub(",",".").to_f,
                            row[D_CANTIDAD].text.gsub("$", "").gsub(".","").gsub(",",".").to_f,
                            row[D_MEASURE_UNIT].text.to_i,
                            row[D_TOTAL].text.gsub("$", "").gsub(".","").gsub(",",".").to_f
                           )
    end
    # metodos de store de datos en la estructura
    def add_orden_compra(orden_compra, sub_jurisdiction)
      if proveedor = sub_jurisdiction.get_proveedor(orden_compra.proveedor)
        proveedor.add_orden_compra(orden_compra)
      end
    end
    def get_subjurisdiction(sub_jurisdiction_id)
      # NO esta bien acceder a una variable de instancia
      @data_result.select{|sj| sj.id == sub_jurisdiction_id}.first
    end
    def has_subjurisdiction?(sub_jurisdiction_id)
      # NO esta bien acceder a una variable de instancia
      @data_result.map(&:id).include?(sub_jurisdiction_id)
    end
    def add_subjurisditcion(sub_jurisdiction)
      # NO esta bien acceder a una variable de instancia
      @data_result << sub_jurisdiction unless has_subjurisdiction?(sub_jurisdiction.id)
    end

    # metodos de parseo del source HTML
    def get_subjurisdiction_id_key(subjurisdiction_id)
      "div#solicitudes#{subjurisdiction_id[1..-1]} table tr"
    end
    def get_subjurisdiction_id(item)
      item.css('input').attr('id').value
    end
    def has_data?(description, kind)
      description.text == "Resumen de Ordenes de Compra No se encontraron ordenes de compra para la b\u00FAsqueda efectuada. " if kind == "OrdenCompra"
      description.text != "DETALLE DE LA ORDEN DE COMPRA" if kind == "DetalleOrdenCompra"
      description.text.gsub("\n","").gsub("\t","").gsub("\r","") != "Raz\u00F3n socialCUIT" if kind == "providers"
    end

    # struct new
    def create_provider(row)
      id = ""
      if (row[PROVIDER_RAZON_SOCIAL] && id = row[PROVIDER_RAZON_SOCIAL].css('a').first)
        id = id["href"].split("=")[1] if id["href"]
      end
      Proveedor.new(id, row[PROVIDER_RAZON_SOCIAL].text, [], row[PROVIDER_CUIT].text)
    end
    def create_sub_jurisdictions(id, name)
      subJurisdiction             = SubJurisdiction.new
      subJurisdiction.id          = id
      subJurisdiction.name        = name
      subJurisdiction.proveedores = []                  # no llama initializer
      subJurisdiction
    end
    def create_ordenes_compra(row)
      OrderCompra.new(row[HEADER_NROOC].text.to_i,
                       Date.strptime(row[HEADER_FECHAEMISION].text,"%d/%m/%Y"),
                       row[HEADER_IMPORTE].text.gsub("$", "").gsub(".","").gsub(",",".").to_f,
                       row[HEADER_PROVEEDOR].text,
                       row[HEADER_TIPOCONTRATACION].text,
                       row[HEADER_OBS].text,
                       row[HEADER_ESTADO].text
                      ) if row.size > 0
    end
    def create_presupuesto(line)
      Presupuesto.new(line[P_DESCRIPCION],
                      line[P_PROCEDENCIA],
                      line[P_ESTIMADO],
                      line[P_ESTIMADO],
                      line[P_VIGENTE],
                      line[P_DEVENGADO],
                      line[P_RECIBIDO])
    end
    def create_order_compra_detail(row)
      OrderCompraDetail.new(row[D_ITEM_NUMBER].text,
                            row[D_DESCRIPTION].text,
                            row[D_UNIT_PRICE].text.gsub("$", "").gsub(".","").gsub(",",".").to_f,
                            row[D_CANTIDAD].text.gsub("$", "").gsub(".","").gsub(",",".").to_f,
                            row[D_MEASURE_UNIT].text.to_i,
                            row[D_TOTAL].text.gsub("$", "").gsub(".","").gsub(",",".").to_f)
    end

    # struct
    Presupuesto = Struct.new(:description, :procedencia, :estimado, :modificacion, :vigente, :devengado, :recibido)
    OrderCompra = Struct.new(:nrooc, :fechaemision, :importe, :proveedor, :tipocon, :obs, :estado)
    OrderCompraDetail = Struct.new(:item_number, :description, :unit_price, :amount, :unit, :total)
    SubJurisdiction = Struct.new(:id, :name, :proveedores) do
      def initialize
        proveedores = []
      end
      def add_proveedor(proveedor)
        proveedores << proveedor unless has_proveedor?(proveedor)
      end
      def has_proveedor?(proveedor)
        proveedor.id = Time.now.utc.to_f unless proveedor.id
        proveedores.include?(proveedor.id)
      end
      def ordenes_compra_count
        proveedores.map(&:ordenes_compra).map(&:size).reduce(:+)
      end
      def total_importe
        proveedores.map(&:total_importe).reduce(:+)
      end
      def get_proveedor(name)
        unless proveedor = proveedores.detect{|p| p.name == name}
          proveedor                 = Proveedor.new
          proveedor.id              = Time.now.utc.to_f # no llama initializer
          proveedor.ordenes_compra  = []                # no llama initializer
          proveedor.name            = name
          # NO esta bien user self para agregar el proveedor.
          self.proveedores << proveedor
        end
        proveedor
      end
    end
    Proveedor = Struct.new(:id, :name, :ordenes_compra, :cuit) do
      # def initialize
      #   id = Time.now.utc.to_f
      #   ordenes_compra  = []
      # end
      def ordenes_compra_count
        ordenes_compra.size
      end
      def add_orden_compra(orden_compra)
        ordenes_compra << orden_compra unless has?(orden_compra)
      end
      def total_importe
        ordenes_compra.map(&:importe).reduce(:+)
      end
      def has?(orden_compra)
        ordenes_compra.include?(orden_compra)
      end
    end
  end
end
