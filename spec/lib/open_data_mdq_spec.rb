require 'spec_helper'
describe OpenDataMdq do
  # it "should be true" do
  #   expect(true).to be(true)
  # end

  describe "HttpReader Detalle Orden Compra" do
    before(:all) do
      @http_reader = OpenDataMdq::HttpReader.new({:year => 2013, :nrooc => "806")
      @data = @http_reader.process_order_detail
    end
    it "shoudl be a FileReader kind" do
      expect(@http_reader).to be_a_kind_of(OpenDataMdq::HttpReader)
    end
    it "should get the data from file" do
      expect(@http_reader.data_result).to be_a_kind_of(Array)
    end
    it "should get one subjurisdiction" do
      expect(@http_reader.data_result.size).to be(1)
    end
  end

  describe "HttpReader Odenes de Compra" do
    before(:all) do
      @http_reader = OpenDataMdq::HttpReader.new({:year => 2013, :sub_jurisdiction_id => "1110200000"})
      @data = @http_reader.process_orders
    end
    it "shoudl have data" do
      expect(@http_reader.data_result).to be_a_kind_of(Array)
    end
  end

  describe "FileReader Detalle Orden Compra" do
    before(:all) do
      @file_reader = OpenDataMdq::FileReader.new({:year => 2013, :nrooc => "806", :source_path => "#{Dir.pwd}/lib/open_data_mdq/data/", :filename => "detalle_multiple.html"})
      @data = @file_reader.process_order_detail
    end
    it "shoudl be a FileReader kind" do
      expect(@file_reader).to be_a_kind_of(OpenDataMdq::FileReader)
    end
    it "should get the data from file" do
      expect(@file_reader.data_result).to be_a_kind_of(Array)
    end
    it "should get one subjurisdiction" do
      expect(@file_reader.data_result.size).to be(3)
    end
  end

  describe "FileReader Providers Page Count" do
    before(:all) do
      @file_reader = OpenDataMdq::FileReader.new({:source_path => "#{Dir.pwd}/lib/open_data_mdq/data/", :filename => "providers.html"})
      @data = @file_reader.get_providers_page_count
    end
    it "should get providers page count" do
      expect(@file_reader.data_result).to be_a_kind_of(Integer)
    end
    it "should get providers page count value" do
      expect(@file_reader.data_result).to be(180)
    end
  end

  describe "HttpReader Odenes de Compra" do
    before(:all) do
      @http_reader = OpenDataMdq::HttpReader.new({:year => 2013, :sub_jurisdiction_id => "1110200000"})
      @data = @http_reader.process_orders
    end
    it "shoudl have data" do
      expect(@http_reader.data_result).to be_a_kind_of(Array)
    end
  end

  describe "HttpReader Providers" do
    before(:all) do
      @http_reader = OpenDataMdq::HttpReader.new({:providers_page_count => 2})
      @data = @http_reader.process_providers
    end
    it "should get current screen providers" do
      expect(@data[:providers]).to be_a_kind_of(Array)
    end
    it "should get 10 providers" do
      expect(@data[:providers].size).to be(20)
    end

  end

  describe "FileReader Providers" do
    before(:all) do
      @file_reader = OpenDataMdq::FileReader.new({:source_path => "#{Dir.pwd}/lib/open_data_mdq/data/", :filenames => ["providers.html", "providers_2.html"]})
      @data = @file_reader.process_providers
    end
    it "should get current screen providers" do
      expect(@data[:providers]).to be_a_kind_of(Array)
    end
    it "should get 10 providers" do
      expect(@data[:providers].size).to be(20)
    end
  end

  describe "FileReader Ordenes Compra" do
    before(:all) do
      @file_reader = OpenDataMdq::FileReader.new({:year => 2013, :sub_jurisdiction_id => "1110200000", :source_path => "#{Dir.pwd}/lib/open_data_mdq/data/", :filename => "datos.html"})
      @data = @file_reader.process_orders
    end
    it "should be a FileReader kind" do
      expect(@file_reader).to be_a_kind_of(OpenDataMdq::FileReader)
    end
    it "should get the data from file" do
      expect(@file_reader.data_result).to be_a_kind_of(Array)
    end
    it "should get one subjurisdiction" do
      expect(@file_reader.data_result.size).to be(1)
    end
    it "should have three providers" do
      expect(@file_reader.data_result.first.proveedores.size).to be(3)
    end
    it "should have 5 ordenes_compra" do
      expect(@file_reader.data_result.first.proveedores.map(&:ordenes_compra).map(&:size).reduce(:+)).to be(5)
    end
    it "should get the total of importe" do
      expect(@file_reader.data_result.first.total_importe).to eq(35051.0)
    end
    it "should get the total number of orders" do
      expect(@file_reader.data_result.first.ordenes_compra_count).to eq(5)
    end
  end
end
