# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'open_data_mdq/version'

Gem::Specification.new do |spec|
  spec.name          = "openDataMDQ"
  spec.version       = OpenDataMdq::VERSION
  spec.authors       = ["Agustin Vinao"] #Vi\xC3\xB1ao
  spec.email         = ["agustinvinao@gmail.com"]
  spec.description   = %q{Gema para extraccion de datos del sitio de la municipalidad de general pueyrredon}
  spec.summary       = %q{Extractor de datos de la municipalidad de general pueyrredon}
  spec.homepage      = "http://opendatamdq.com.ar"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "nokogiri", "~> 1.5.5"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
end
